//Mengatasi template class error
import android.support.annotation.NonNull;Go to File->setting->editor->file and code templates, select Class in the files tab and paste this:
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end #parse("File Header.java") public class ${NAME} { }

//set drawable textview
Drawable draw = getResources().getDrawable(R.drawable.i_clear);
            draw.setBounds(0,0,40,40);
            txt_hasil.setCompoundDrawables(null,null,draw,null);
            
//SQLite helper
public class dbHelp extends SQLiteOpenHelper {
    public dbHelp(Context context){
        super(context,"db_msg",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

//SQLite CREATE_TABLE
mQuery = "CREATE TABLE IF NOT EXISTS tb_"+ pubID +" (" +
                "_id INT,"+
                "chat_msg TEXT,"+
                "chat_time TEXT,"+
                "chat_status TEXT)";
                
//SQLite INSERT_TABLE
String sendQ = "INSERT INTO tb_"+ userID +
                " VALUES (1,\""+ msg +"\",\""+ time +"\",\"R\")";

//UPDATE TABLE
mQuery = "UPDATE tb_listchat SET 
            list_msg = \""+ msg +"\", 
            list_jam =\""+ time +"\" "+
            "WHERE list_ID=\""+ pubID +"\"";
db.execSQL(mQuery);

//DELETE ROW
Query = "DELETE FROM tb_setting WHERE setting_request='26' AND _id='2'";
        db.execSQL(Query);

//tanggal dan waktu
Calendar call = Calendar.getInstance();
        String hour = String.valueOf(call.get(Calendar.HOUR_OF_DAY));
        String min = String.valueOf(call.get(Calendar.MINUTE));

//Cursor adapter database to listview
public class CursorAdapterListChat extends CursorAdapter {
    public CursorAdapterListChat(Context context, Cursor cursor){
        super(context,cursor,0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent){
        return LayoutInflater.from(context).inflate(R.layout.row_listchat,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor){
        TextView txt_nama = (TextView) view.findViewById(R.id.txt_nama);
        TextView txt_msg = (TextView) view.findViewById(R.id.txt_msg);
        TextView txt_jam = (TextView) view.findViewById(R.id.txt_jam);
        ImageView img_photo = (ImageView) view.findViewById(R.id.img_photo);

        String nama = cursor.getString(cursor.getColumnIndexOrThrow("list_nama"));
        String msg = cursor.getString(cursor.getColumnIndexOrThrow("list_msg"));
        String jam = cursor.getString(cursor.getColumnIndexOrThrow("list_jam"));
        String uri_photo = cursor.getString(cursor.getColumnIndexOrThrow("list_photo"));

        txt_nama.setText(nama);
        txt_msg.setText(msg);
        txt_jam.setText(jam);
        Picasso.with(context).load(uri_photo).into(img_photo);
    }
}

public void showMessage(){
        SQLiteDatabase db = helper.getWritableDatabase();

        mQuery = "SELECT user_publish FROM tb_user";
        Cursor c = db.rawQuery(mQuery,null);
        c.moveToFirst();
        String pubID = c.getString(0);

        mQuery = "SELECT list_nama FROM tb_listchat " +
                "WHERE list_ID = \""+ pubID +"\"";
        c = db.rawQuery(mQuery,null);
        c.moveToFirst();
        String nama = c.getString(0);
        this.getSupportActionBar().setTitle(nama);

        mQuery = "SELECT * FROM tb_"+ pubID;
        c = db.rawQuery(mQuery,null);
        c.moveToFirst();
        com.nyiur.oteach.CursorAdapterChat chatAdapter = new com.nyiur.oteach.CursorAdapterChat(this,c);
        contentlist.setAdapter(chatAdapter);
    }
    
//Color drawable xml
<shape xmlns:android="http://schemas.android.com/apk/res/android">
    <size android:width="3dp"/>
    <solid android:color="#00685C"/>
</shape>

//cek running service
  boolean checkServices(){
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if (service.service.getClassName().equals("com.anksite.autocekpulsa.ServiceAccess")){
                return true;
            }
        }return false;
    }
    
//Intent Accestibility setting
 Intent i = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
 
//AlarmManager BroadcastReceiver
void setAlarm(){
        int jam = tp_jam.getCurrentHour();
        int menit = tp_jam.getCurrentMinute();

        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.set(java.util.Calendar.HOUR_OF_DAY,jam);
        calendar.set(java.util.Calendar.MINUTE,0);
        calendar.set(java.util.Calendar.SECOND,0);

        Intent i = new Intent(this, ReceiverAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(this,1998,i,0);

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+10000,pi);
        Toast.makeText(thisACtivity, "ALarm Set", Toast.LENGTH_SHORT).show();
    }
    
public class ReceiverAlarm extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Receiver Bekerja !!!!!!", Toast.LENGTH_SHORT).show();
        String ussd = "*123#";
        if (ussd.contains("#")){
            ussd = ussd.substring(0,ussd.indexOf("#")) + Uri.encode("#");
            Log.d("call", ussd);
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        int permissionCheck = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CALL_PHONE);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED){
            context.startActivity(callIntent);
        }
    }
}

//AlertDialog
 AlertDialog.Builder builder = new AlertDialog.Builder(CekNomor.this);
                builder.setMessage("Laporkan data untuk pengambangan aplikasi ini?")
                        .setIcon(R.drawable.ic_launcher);
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
                        mRootRef.child(nomor).setValue("prefix salah");
                        Toast.makeText(CekNomor.this, "Terimakasih atas laporannya :)", Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog allert = builder.create();
                allert.show();
				
//inflate layout to alert dialog
 android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_Judul));
        final View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_judul, (ViewGroup) findViewById(android.R.id.content), false);
        builder.setView(viewInflated);

        edt_judul = (EditText) viewInflated.findViewById(R.id.edt_judul);

        edt_judul.setText(txt_judulChild.getText());

        builder.setPositiveButton(getString(R.string.opsi_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                txt_judulChild.setText(edt_judul.getText());
            }
        });

//Compile material Cardview FAB
compile 'com.android.support:design:24.2.0'
compile 'com.android.support:cardview-v7:24.2.0'

//Get item position ListView
public void deleteSch(View v){
        View lin1 = (View) v.getParent();
        View lin2 = (View) lin1.getParent();
        ListView list_schedule = (ListView) lin2.getParent();
        int position = list_schedule.getPositionForView(lin2);
    }
    
//background selector xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_height="wrap_content"
    android:layout_width="wrap_content">
    <item android:state_pressed="true" android:drawable="@drawable/color_lapor_pressed"/>
    <item android:drawable="@drawable/color_lapor"/>
</selector>

//color xmln
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle"
    android:layout_height="wrap_content"
    android:layout_width="wrap_content">
    <solid android:color="#e53935"/>
    <corners android:radius="5dp"/>
</shape>

//Shared Preferences
 @Override
    public void onStop(){
        super.onStop();
        SharedPreferences settings;
        settings = getSharedPreferences("clickSave", 0);
        //set the sharedpref
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("click",click);
        Log.d("menyimpan click",String.valueOf(click));
        editor.commit();
    }
    
     //Get Click value from sharedpref
        SharedPreferences settings;
        settings = getSharedPreferences("clickSave", 0);
        //get the sharedpref
        //0 adalah default value atau nilai awal
        click = settings.getInt("click",0);
        Log.d("nilai click",String.valueOf(click));

//header ListView
 View space = getLayoutInflater().inflate(R.layout.header_schedule,null);
        list_schedule.addHeaderView(space);

//runtime permission
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CALL_PHONE);

        //Jika sudah ada izin, langsung eksekusi
        if (permissionCheck == PackageManager.PERMISSION_GRANTED){
            startActivity(callIntent);
            Toast.makeText(thisActivity, "Melakukan panggilan USSD (onGranted)", Toast.LENGTH_SHORT).show();
        }
        //Belum dapat permission
        else if(permissionCheck == PackageManager.PERMISSION_DENIED){
            //Jika apernah menolak, beri penjelasan
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)){
                permNote();
            }
            //Tampilkan dialog permission
            else{
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},1998);
            }
        }

          //saat user mengizinkan
          @Override
            public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                if (requestCode==1998 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    startActivity(callIntent);
                    Toast.makeText(thisActivity, "Melakukan pangglan USSD (onRequestResult)", Toast.LENGTH_SHORT).show();
                }
            }

//firebase
//project
 dependencies {
        classpath 'com.android.tools.build:gradle:2.2.2'
        classpath 'com.google.gms:google-services:3.0.0'


//module app
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    androidTestCompile('com.android.support.test.espresso:espresso-core:2.2.2', {
        exclude group: 'com.android.support', module: 'support-annotations'
    })
    compile 'com.android.support:appcompat-v7:24.2.1'
    compile 'com.google.firebase:firebase-database:10.0.1'
    compile 'com.google.firebase:firebase-ads:10.0.1'
    compile 'com.google.firebase:firebase-crash:10.0.1'
    testCompile 'junit:junit:4.12'
}

apply plugin: 'com.google.gms.google-services'

//Test add unit id
interstitial: ca-app-pub-3940256099942544/1033173712
banner: ca-app-pub-3940256099942544/6300978111
video: ca-app-pub-3940256099942544/5224354917

//BANNER AD
        ads:adSize="BANNER"
        ads:adUnitId="@string/add_banner"

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int error){
                mAdView.setVisibility(View.GONE);
            }
        });

//INTERSTITIAL AD
  private InterstitialAd fullAdd;
        fullAdd = new InterstitialAd(this);
        fullAdd.setAdUnitId(getResources().getString(R.string.interstitial_CekOperator));
        fullAdd.loadAd(adRequest);
        fullAdd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                AdRequest adRequest = new AdRequest.Builder().build();
                fullAdd.loadAd(adRequest);
            }
        });

         if (fullAdd.isLoaded()){
                                click = 0;
                                fullAdd.show();

//VIDEO AD
///publiv variabel
class MainActivity : AppCompatActivity(), RewardedVideoAdListener {
private lateinit var mRewardedVideoAd: RewardedVideoAd
 /// Use an activity context to get the rewarded video instance.
mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this)
mRewardedVideoAd.rewardedVideoAdListener = this
loadRewardedVideoAd()

private fun loadRewardedVideoAd() {
    mRewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917",
            AdRequest.Builder().build())
}

///event video AD
override fun onRewarded(reward: RewardItem) {
    Toast.makeText(this, "onRewarded! currency: ${reward.type} amount: ${reward.amount}",
            Toast.LENGTH_SHORT).show()
    // Reward the user.
}

override fun onRewardedVideoAdLeftApplication() {
    Toast.makeText(this, "onRewardedVideoAdLeftApplication", Toast.LENGTH_SHORT).show()
}

override fun onRewardedVideoAdClosed() {
    Toast.makeText(this, "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show()
}

override fun onRewardedVideoAdFailedToLoad(errorCode: Int) {
    Toast.makeText(this, "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show()
}

override fun onRewardedVideoAdLoaded() {
    Toast.makeText(this, "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show()
}

override fun onRewardedVideoAdOpened() {
    Toast.makeText(this, "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show()
}

override fun onRewardedVideoStarted() {
    Toast.makeText(this, "onRewardedVideoStarted", Toast.LENGTH_SHORT).show()
}

///Display video ad
if (mRewardedVideoAd.isLoaded) {
    mRewardedVideoAd.show()
}
        
//copy string
  ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copy: ",msg);
                clipboard.setPrimaryClip(clip);
                
//memastikan install dari play store
 void checkInstaller(){
        PackageManager pm = getPackageManager();
        String source = pm.getInstallerPackageName(getPackageName());
        Toast.makeText(this, source, Toast.LENGTH_SHORT).show();
    }


//fragment transaction
 final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.from_left_slide_in, R.anim.from_left_slide_out);
            ft.replace(R.id.content_frame, fragment, "ket");
            ft.commit();

//simple fragment transaction
supportFragmentManager.beginTransaction().replace(R.id.fl_main, FragmentListMatchContainer()).commit()

//Volley json parsing
implementation 'com.android.volley:volley:1.0.0'

RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/baking.json";
        final StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                int JSONcount = 0;
                try {
                    menu = new JSONArray(response);
                    JSONcount = menu.length();
                }catch (JSONException e){
                    e.printStackTrace();
                }
                AdapterMenu adapterMenu = new AdapterMenu(JSONcount,listener,response,mContect);
                GridLayoutManager layoutManager = new GridLayoutManager(mContect,numberOfColumns());
                rv_main.setLayoutManager(layoutManager);
                rv_main.setAdapter(adapterMenu);
                pb_loading.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContect, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(request);
    }

void getData(){
RequestQueue queue = Volley.newRequestQueue(this);
        String url = ".....///link.php";
        final StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("respon", response)
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContect, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(request);
    }

}


//Membuat koneksi JDBC di Oracle
 public class Koneksi {
	
	public Connection getOracleDBConnection(String var_name, String var_port, String var_sid, String var_uname, String var_pass) {
		 
		Connection connection = null;
        try {

      	  // Load the Oracle JDBC driver
      String driverName = "oracle.jdbc.driver.OracleDriver";
      Class.forName(driverName);
      	  // Create a connection to the database
      String serverName = var_name;
      String serverPort = var_port;
      String sid = var_sid;
      String url = "jdbc:oracle:thin:@" + serverName + ":" + serverPort + ":" + sid;
      String username = var_uname;
      String password = var_pass;
            Log.d("Log Conn","URL: "+url+" Username: "+username+" Password: "+password);
      	  		connection = DriverManager.getConnection(url, username, password);
      System.out.println("Successfully Connected to the database!");
   
            
          } catch (Exception e) {
           
            e.printStackTrace();
          
    }
 
		return connection;
 
	}
}

//auto complete EditText
// Get a reference to the AutoCompleteTextView in the layout
AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autocomplete_country);
// Get the string array
String[] countries = getResources().getStringArray(R.array.countries_array);
// Create the adapter and set it to the AutoCompleteTextView
ArrayAdapter<String> adapter =
        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, countries);
textView.setAdapter(adapter);

//cek nilai array
Arrays.asList(array_tes).contains("sayambara")  

//alert dialog pilih item
AlertDialog.Builder builder = new AlertDialog.Builder(ListItem.this);
				                builder.setTitle("Jenis Material Transaction");
				                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener()
								
//Date format
SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
Date date = fmt.parse("2013-05-06");

//bandingkan tanggal compare date
if(date1.after(date2)){
System.out.println("Date1 is after Date2");
}

if(date1.before(date2)){
System.out.println("Date1 is before Date2");
}

if(date1.equals(date2)){

}

//Compare 2 
SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
Date strDate = sdf.parse(valid_until);
if (new Date().after(strDate)) {
    catalog_outdated = 1;
}

final String OLD_FORMAT = "dd/MM/yyyy";
final String NEW_FORMAT = "yyyy/MM/dd";

//timestamp
String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());


//merubah format tanggal
val date = SimpleDateFormat("yyy-MM-dd").parse(strDate)
val display = SimpleDateFormat("EEE, dd MMM yyy").format(date)

// August 12, 2010
String oldDateString = "12/08/2010";
String newDateString;

SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
Date d = sdf.parse(oldDateString);

sdf.applyPattern(NEW_FORMAT);
newDateString = sdf.format(d);
                System.out.println("Date1 is equal Date2");
            }

//disable listview click
<ListView 
android:listSelector="@android:color/transparent" 
android:cacheColorHint="@android:color/transparent"
/>

//shared preferences session user
class ManagerSessionUserOracle {
    private SharedPreferences pref;
    private static final  String PREF_NAME = "session";
    private final String KEY_IS_LOGIN = "isLogin";

    final String KEY_USERNAME = "username";
    final String KEY_PASSWORD = "password";

    ManagerSessionUserOracle(Context mContext){
        pref = mContext.getSharedPreferences(PREF_NAME,0);//PrivateMode
    }

    void createUserSession(String username, String password){
        SharedPreferences.Editor edit;
        edit = pref.edit();

        edit.putBoolean(KEY_IS_LOGIN,true);
        edit.putString(KEY_USERNAME, username);
        edit.putString(KEY_PASSWORD, password);

        edit.apply();
    }

    HashMap<String, String> getUserSession(){
        HashMap<String, String> userData = new HashMap<>();

        userData.put(KEY_USERNAME, pref.getString(KEY_USERNAME,null));
        userData.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD,null));

        return userData;
    }

    Boolean isUserLogin(){
        return pref.getBoolean(KEY_IS_LOGIN,false);
    }
}

//save to session and get data
ManagerSessionUserOracle session = new ManagerSessionUserOracle(this);
session.createUserSession($username, $password);

    void getSession(){
        ManagerSessionUserOracle session = new ManagerSessionUserOracle(this);
        HashMap<String, String> userData = session.getUserSession();

        String username = userData.get(session.KEY_USERNAME);
        String password = userData.get(session.KEY_PASSWORD);

        tv_session.setText(username+" AND "+password);
    }

//handler
Handler h = new Handler();
    int delay = 15000; //15 seconds
    Runnable runnable;
    @Override
    protected void onStart() {
//start handler as activity become visible

        h.postDelayed(new Runnable() {
            public void run() {
                //do something

                runnable=this;

                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onStart();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

//SQL EXCEPTION
 try {

        } catch (SQLException e) {
            e.printStackTrace();
        }
		
//Check alarmManager
adb shell dumpsys alarm

//SQLquery format date
select TO_CHAR(TO_DATE('01-FEB-2011'), 'DAY, DD MONTH YYYY'
    ,'nls_date_language = INDONESIAN') from dual;
	
//ALTER PLSQL
ALTER TABLE customers
  MODIFY customer_name varchar2(100) NOT NULL;
 
ALTER TABLE customers
  ADD city varchar2(40) DEFAULT 'Seattle';
  
ALTER TABLE table_name
  RENAME COLUMN old_name TO new_name;
		

//Spinner adapter
 ArrayAdapter<String> adapter= new ArrayAdapter<String>(context, R.layout.spinner_item, myList);
 adapter.setDropDownViewResource(R.layout.spinner_item);
 
//fullscreen activity
//According to grepcode, this method has been present but hidden since version 4.1.1. 
//So you can annotate the method that calls it as @TargetApi(Build.VERSION_CODES.LOLLIPOP), 
//and protect the call itself with if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN).
requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

//Load image from assets folder
try 
{
    // get input stream
    InputStream ims = getAssets().open("avatar.jpg");
    // load image as Drawable
    Drawable d = Drawable.createFromStream(ims, null);
    // set image to ImageView
    mImage.setImageDrawable(d);
  ims .close();
}
catch(IOException ex) 
{
    return;
}

//Listview from array list
public class YourActivity extends Activity {

    private ListView lv;

    public void onCreate(Bundle saveInstanceState) {
         setContentView(R.layout.your_layout);

         lv = (ListView) findViewById(R.id.your_list_view_id);

         // Instanciating an array list (you don't need to do this, 
         // you already have yours).
         List<String> your_array_list = new ArrayList<String>();
         your_array_list.add("foo");
         your_array_list.add("bar");

         // This is the array adapter, it takes the context of the activity as a 
         // first parameter, the type of list view as a second parameter and your 
         // array as a third parameter.
         ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                 this, 
                 android.R.layout.simple_list_item_1,
                 your_array_list );

         lv.setAdapter(arrayAdapter); 
    }
}

//Use stetho
implementation 'com.facebook.stetho:stetho:1.5.0'
Stetho.initializeWithDefaults(this);

//recyclerview animation appear
private void setFadeAnimation(View view) {
    AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
    anim.setDuration(FADE_DURATION);
    view.startAnimation(anim);
}

private void setScaleAnimation(View view) {
    ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    anim.setDuration(FADE_DURATION);
    view.startAnimation(anim);
}

  private void setAnimation(View viewToAnimate, int position) {

            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;

    }
	
//recyclerview animation one by one
    private void setAnimation(final View view) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
                if(view!=null){
                    view.startAnimation(animation);
                    view.setVisibility(View.VISIBLE);
                }
            }
        }, delayAnimate);
        delayAnimate+=300;
    }
	
//Read string from nput
String job = readStream(mResult.getAsciiStream(1));

private String readStream(InputStream data){
       Scanner s = new Scanner(data).useDelimiter("\\A");
       return s.hasNext() ? s.next() : "";
}

//create recycler view 
public class RecyclerMoveTrx extends RecyclerView.Adapter<RecyclerMoveTrx.ViewHolderMoveTrx>{
    private Context mContext;
    private ResultSet mResult;
    final private ItemClickListener mItemClickListener;
    Handler handler;
    Runnable runnable;
    int delayAnimate=300;

    public RecyclerMoveTrx(ItemClickListener listener){
        //put data here
        mItemClickListener = listener;
    }

    public interface ItemClickListener{
        void OnItemClick(String jobName);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public ViewHolderMoveTrx onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate view row
        return new ViewHolderMoveTrx(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderMoveTrx holder, int position) {
        //set data to holder.view
		//set animation here
    }

    class ViewHolderMoveTrx extends RecyclerView.ViewHolder implements AdapterView.OnClickListener{
        public ViewHolderMoveTrx(View itemView){
            super(itemView);
			//colect view here
            //itemView.findViewById
            viewRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
			mItemClickListener.OnItemClick(data);
	
        }
    }
	//Langkah terakhir adalah implement interface di activity yang menampilkan recyclerview
}

//alert dialog message format html
String msg = "<b>Material kurang</b><br>" +
                        "Item  &nbsp: "+itemName+"<br>" +
                        "Qty &nbsp;&nbsp;&nbsp: "+itemAvail+"<br>"+
                        "Desc : "+itemDesc;
builder.setMessage(Html.fromHtml(msg))

//get ip address
WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
		
//animasi hanya tampil saat bind awal
            if (delayAnimate <= (itemCount+1)*300){
                holder.ll_root.setVisibility(View.INVISIBLE);
                setAnimation(holder.ll_root);
            }
			
//factory reset
fastboot erase userdata
fastboot erase cache

//picasso
compile 'com.squareup.picasso:picasso:2.5.2'

//Picasso load ti image view
Picasso  
    .with(context)
    .load(UsageExampleListViewAdapter.eatFoodyImages[0])
    .fit()
    // call .centerInside() or .centerCrop() to avoid a stretched image
    .into(imageViewFit);

//EditText enter listener
        et_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    itemName = et_code.getText().toString();
                    et_code.setEnabled(false);
                    et_code.setEnabled(true);
                    createRecyclerView(itemName);
                    return true;
                }
                return false;
            }
        });
		
//insert SQLite content values
ContentValues values = new ContentValues();
values.put(helper.PO_C1_PO_HEADER_ID	       ,mResult.getString(1));
helper.insertDataPo(values);

void insertDataMo(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("Values",""+values.toString());
        db.insert(TABLE_MO,null,values);
    }
	
//get all contact list
void getContact(){
        String names="";

        Cursor people = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null,null,null,
                null);

        while (people.moveToNext()) {


            int i=people.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
            int i2=people.getColumnIndex(ContactsContract.PhoneLookup._ID);
            String name=people.getString(i);
            String id1=people.getString(i2);
            names+=name+":";
            Uri uri2 = ContactsContract.
                    CommonDataKinds.Phone.CONTENT_URI;
            String[] projectio = new String[] {
                    ContactsContract.CommonDataKinds.
                            Phone.NUMBER };
            String selectio= ContactsContract.
                    CommonDataKinds.Phone.CONTACT_ID +
                    "=?";
            String[] selectionArg = new String[]
                    {id1 };
            Cursor peopl=getContentResolver().query(uri2,null,selectio,selectionArg,null);
            while(peopl.moveToNext())
            {
                int i3=peopl.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                String phonenum=peopl.getString(i3);
                names+=phonenum+"\n";


            }

        }
        Log.d("names",names);
    }
	
//create table id auto increment
mQuery = "CREATE TABLE IF NOT EXISTS tb_"+ pubID +" (" +
                "_id INTEGER PRIMARY KEY,"+
                "chat_msg TEXT,"+
                "chat_time TEXT,"+
                "chat_status TEXT)";
				
//replace String
String packing = list.get(i);
String displayPack;
displayPack = packing.replace("kk", "Kardus Kecil ");]

//Callable statement 
CallableStatement funcnone = dbConnection.prepareCall
("BEGIN Fnd_Global.apps_initialize(" + userId + ",50630,20003); :x := apps.Fnd_Request.submit_request('INV','INCTCM','Interface Inventory Transactions'); END;");
funcnone.registerOutParameter(1, Types.INTEGER);
funcnone.executeUpdate();
System.out.println("Return value is : " + funcnone.getInt(1));
funcnone.close();

//membuat menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }
	
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@+id/menu_logout"
        android:title="Logout"/>
</menu>

//kembali ke tampilan awal aplikasi
// After logout redirect user to Login Activity
Intent i = new Intent(_context, login_activity.class);
// Closing all the Activities
i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
// Add new Flag to start new Activity
i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
// Staring Login Activity
_context.startActivity(i);

Intent i = new Intent(this, ActivityLogin.class);
i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
startActivity(i);

//library chart
https://github.com/PhilJay/MPAndroidChart

//mendapat hasil scann
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (requestCode == 0) {
            Log.d("Result", ""+requestCode);
            if (resultCode == RESULT_OK) {
                final String contents = data.getStringExtra("SCAN_RESULT");
                et_code.setText(contents);
                processMO();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(ActivityScan.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }
	
//Change icon color bottom navigation
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:state_checked="true" android:color="@android:color/white" />
    <item android:state_pressed="true" android:state_enabled="true" android:color="@android:color/white" />
    <item android:color="@color/InactiveBottomNavIconColor" />
	
<android.support.design.widget.BottomNavigationView
        app:itemIconTint="@color/bottom_nav_icon_color_selector"
        app:itemTextColor="@color/bottom_nav_icon_color_selector"
		app:itemBackground="@color/silver"/>
		
//Checked background color
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:state_checked="true" android:color="@android:color/holo_blue_dark" />
    <item android:color="@android:color/darker_gray"  />
</selector>

//progressbar background
SDK_path\platforms\android19\data\res\drawable\progress_horizontal.xml
"progress color" and "background color".

//kotlin SQLiteOpenHelper
class dbHelp(context: Context) : SQLiteOpenHelper(context, "db_msg", null, 1) {

    override fun onCreate(p0: SQLiteDatabase?) {
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
    }
}

//kotlin global variabel
lateinit var mQuery : String

//kotlin intent new activity
val i = Intent(this, ActivityHitungBangun::class.java)
startActivity(i)

//Download file using webview
webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request( Uri.parse(urlDownload));
				request.allowScanningByMediaScanner();
				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
				request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "filename.pdf");
				DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
				dm.enqueue(request);
            }
        });

//allow network on main thread
StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
StrictMode.setThreadPolicy(policy);

//display gif animation 
compile 'com.github.bumptech.glide:glide:4.0.0'
Glide.with(context).load(GIF_URI).into(new GlideDrawableImageViewTarget(IMAGE_VIEW));

//insert date to sql
insert into table_name
(date_field)
values
(TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));

//interface date picker
AppCompatActivity implements DatePickerDialog.OnDateSetListener
 @Override
    public void onDateSet(
	
//Koneksi MySqly
Class.forName("com.mysql.jdbc.Driver").newInstance();
String connectionUrl = "jdbc:mysql://quick.com:3306/androiduser_db";
String connectionUser = "personal";
String connectionPassword = "QU1CK123";
Connection conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
System.out.println("Successfully Connected to the database MySQL!");

//get count resultset
mResult.last();
int itemCount = mResult.getRow();

//install swift on linux
https://www.twilio.com/blog/2015/12/getting-started-with-swift-on-linux.html

//get started swift
https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/GuidedTour.html

//multidex enable
defaultConfig {
        applicationId "com.quick.kibbarcode"
        minSdkVersion 16
        targetSdkVersion 25
        multiDexEnabled true
    }

//gambar dari asset
val ism = context?.assets?.open("cards/$key.png")
val drawable = Drawable.createFromStream(ism, null)
iv?.setImageDrawable(drawable)

//Optimize APK size
android {
    defaultConfig {
        resConfigs "en"
    }
    buildTypes {
        release {
            minifyEnabled true
            shrinkResources true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
	
//back action bar
getActionBar().setDisplayHomeAsUpEnabled(true);
onOptionsItemSelected
case android.R.id.home:
            finish();
            return true;
			
//activity result
startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);

Intent intent = new Intent();
        intent.putExtra("keyName", stringToPassBack);
        setResult(Activity.RESULT_OK, intent);
        finish();
		
@Override  
protected void onActivityResult(int requestCode, int resultCode, Intent data)  {  
    super.onActivityResult(requestCode, resultCode, data); 
    if(requestCode==2){  
        String message=data.getStringExtra("MESSAGE");   
    }  
}

//BaseAdapter
 @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v==null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            v = inflater.inflate(R.layout.row_po,null);
        }

        TextView tv_po = (TextView) v.findViewById(R.id.tv_po);
        TextView tv_itemName = (TextView) v.findViewById(R.id.tv_itemName);
        TextView tv_itemDesc = (TextView) v.findViewById(R.id.tv_itemDesc);
        TextView tv_date = (TextView) v.findViewById(R.id.tv_date);

        DataCariPo dataPO = mArrayResult.get(position);

        tv_po.setText(dataPO.mPO);
        tv_itemName.setText(dataPO.mItemName);
        tv_itemDesc.setText(dataPO.mItemDesc);
        tv_date.setText(dataPO.mDate);
        return v;
    }
	
//list view with control dellay
public class CursorAdapterPo extends CursorAdapter {
    int delayAnimate = 500;
    boolean mAnimate;
    int itemCount;
    int bindPosition = 0;
    int cursorPosition;
    Context mContext;
    Handler controlDelay = new Handler();
    int lastGetDelay;
    Runnable runnable;

    public CursorAdapterPo(Context context, Cursor cursor, boolean animate){
        super(context,cursor,0);
        mAnimate = animate;
        itemCount = cursor.getCount();
        mContext = context;
        if(itemCount==0){
            Toast.makeText(context, "No item found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.row_list_barang,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //cursor pos selalu berganti tiap bind
        cursorPosition = cursor.getPosition();

        //bind pos untuk mengetahui berapa item yang sudah di scroll down
        if(cursor.getPosition()>bindPosition){
            bindPosition = cursor.getPosition();
        }
        TextView tv_itemName   = (TextView) view.findViewById(R.id.tv_itemName);
        TextView tv_itemDesc   = (TextView) view.findViewById(R.id.tv_itemDesc);
        TextView tv_qtyAvail   = (TextView) view.findViewById(R.id.tv_qtyAvail);
        TextView tv_qtyReceipt = (TextView) view.findViewById(R.id.tv_qtyReceipt);
        ImageView iv_check     = (ImageView) view.findViewById(R.id.iv_check);

        int qtyAvail = cursor.getInt(4)-cursor.getInt(5);

        tv_itemName.setText(cursor.getString(2));
        tv_itemDesc.setText(cursor.getString(3));
        tv_qtyAvail.setText(""+qtyAvail);
        tv_qtyReceipt.setText(cursor.getString(6));
        if(cursor.getString(7).equals("Y")){
            iv_check.setImageResource(R.drawable.i_check);
        }else{
            iv_check.setImageResource(R.drawable.i_check_disable);
        }

        if (mAnimate){
            //animasi hanya tampil saat bind awal
            Log.d("Cursor Posisi",""+cursorPosition);
            Log.d("Bind Posisi",""+bindPosition);
            if (cursorPosition==bindPosition&&bindPosition<itemCount){
                view.setVisibility(View.INVISIBLE);
                setAnimation(view);
                controlDelay();
                if(bindPosition+1==itemCount){
                    bindPosition+=1;
                }
            }
        }
    }

    private void setAnimation(final View view) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
                if(view!=null){
                    view.startAnimation(animation);
                    view.setVisibility(View.VISIBLE);
                }
            }
        }, delayAnimate);
        delayAnimate+=300;
    }

    //Mengembalikan delay ke 0 jika animasi tidak berjalan
    void controlDelay() {
        controlDelay.postDelayed(new Runnable() {
            @Override
            public void run() {
                runnable = this;
                if (lastGetDelay == delayAnimate) {
                    delayAnimate = 0;
                    controlDelay.removeCallbacks(runnable);
                    Log.d("ControlDelay", "OFF");
                } else {
                    controlDelay.postDelayed(runnable, 600);
                    Log.d("ControlDelay", "ON");
                    lastGetDelay = delayAnimate;
                }
            }
        }, 600);
    }
}

//mengatasi conflict saat compile apache http client jar
packagingOptions {
    exclude 'META-INF/DEPENDENCIES.txt'
    exclude 'META-INF/LICENSE.txt'
    exclude 'META-INF/NOTICE.txt'
    exclude 'META-INF/NOTICE'
    exclude 'META-INF/LICENSE'
    exclude 'META-INF/DEPENDENCIES'
    exclude 'META-INF/notice.txt'
    exclude 'META-INF/license.txt'
    exclude 'META-INF/dependencies.txt'
    exclude 'META-INF/LGPL2.1'
}

//load json from assets
val ism = context.assets.open("json/hero.json")
val size = ism.available()
val buffer = ByteArray(size)
ism.read(buffer)
ism.close()
val json = String(buffer, Charset.forName("UTF-8"))

//Must have todo before upload
-Proguard maping 
=======
//run BOOT_COMPLETE
adb shell
am broadcast -a android.intent.action.BOOT_COMPLETED
adb shell am broadcast -a android.intent.action.BOOT_COMPLETED -p com.quick.gpsapp

am broadcast -a android.intent.action.ANK -p com.quick.gpsapp

//date picker
//buat fragment
public static class FragmentDatePicker extends DialogFragment {
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar c = Calendar.getInstance();
            int date = c.get(Calendar.DATE);
            int month = c.get(Calendar.MONTH);
            int year = c.get(Calendar.YEAR);
            return new DatePickerDialog(getContext(), (ActivityManageHeader) getActivity(), year, month, date);
        }
    }
//implement
DatePickerDialog.OnDateSetListener
//Run
DialogFragment picker = new FragmentDatePicker();
picker.show(getSupportFragmentManager(), "tgl_sp");

//Handle two date picker
View.OnClickListener showDatePicker = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        final View vv = v;

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (vv.getId() == R.id.StartDate //id of your StartDate button) {
                        //do the stuff
                } else //EndDate button was clicked {
                        //do the stuff
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }
};
startDate.setOnClickListener(showDatePicker);
endDate.setOnClickListener(showDatePicker);

//ERROR AS 3.1
configurations.all {
        resolutionStrategy.eachDependency { DependencyResolveDetails details ->
            def requested = details.requested
            if (requested.group == 'com.android.support') {
                if (!requested.name.startsWith("multidex")) {
                    details.useVersion '25.3.0'
                }
            }
        }
    }
	
//get concole message web view
webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                android.util.Log.d("WebView", consoleMessage.message());
                result = consoleMessage.message();
                if (result.length() == 1) {
                    Toast.makeText(WebLoginActivity.this, "Username atau password salah", Toast.LENGTH_SHORT).show();
                } else {
                    SP_Helper.saveSPString(SharedPrefManager.SP_ID, result);
                    SP_Helper.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
                    Intent i = new Intent(WebLoginActivity.this, MainActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
//                    finish();
                }
                return true;
            }
        });

//runtime permission efisien
void runtimePermission(){
    int permissionCheck=ContextCompat.checkSelfPermission(getApplicationContext(),
            Manifest.permission.CAMERA);
    if(permissionCheck==PackageManager.PERMISSION_DENIED){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},1998);
        Log.d("Request","start launch request");
    }else{
        //launch action that require permission
    }
}

@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode==1998 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
        runtimePermission();
    }else{
        Toast.makeText(this, "Izinkan aplikasi mengakses kamera untuk melakukan SCAN", Toast.LENGTH_LONG).show();
    }
}

//retrofit upload image
/*script upload php*/
<?php
    $result = array("success" => $_FILES["file"]["name"]);
    $file_path = basename( $_FILES['file']['name']);
    if(move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {
        $result = array("success" => "File successfully uploaded");
    } else{
        $result = array("error" => "error uploading file");
        $result = array("success" => "error uploading file");
    }
    echo json_encode($result, JSON_PRETTY_PRINT);
?>

/*compile retrofit*/
compile 'com.squareup.retrofit2:retrofit:2.1.0'
compile 'com.squareup.retrofit2:converter-scalars:2.1.0'
compile 'com.squareup.okhttp3:okhttp:3.2.0'
compile 'com.squareup.okio:okio:1.7.0'
compile 'com.google.code.gson:gson:2.6.2'
compile 'com.squareup.retrofit2:converter-gson:2.0.1'

/*create interface upload*/
public interface UploadImageInterface {
    @Multipart
    //@POST("/dataDL/imagefolder/upload.php")
    @POST("/aplikasi/dinas-luar-online/assets/fileupload/kendaraan/upload.php")
    Call<String> uploadFile(@Part MultipartBody.Part file, @Part("name") RequestBody name);
}

/*method upload*/
void uploadImage(){
    String filePath = getRealPathFromURIPath(mUri, ActivityMobil.this);
    File file = new File(filePath);
    mFileName = file.getName();
    Log.d("FILE",mFileName);
    RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
    RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(SERVER_PATH)
            .addConverterFactory(ScalarsConverterFactory.create())
            .build();
    UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
    Call<String> fileUpload = uploadImage.uploadFile(fileToUpload, filename);
    fileUpload.enqueue(new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            Log.d("Call", call.toString());
            Log.d("Response", response.raw().toString());
            //Toast.makeText(ActivityMain.this, "Success " + response.body().getSuccess(), Toast.LENGTH_LONG).show();
            if(response.raw().message().equals("OK")){
                ArrayList<String> listSpdl = getIntent().getStringArrayListExtra("spdlid");
                for(int i = 0; i<listSpdl.size();i++){
                    insertData(listSpdl.get(i));
                }
                Intent i = new Intent(ActivityMobil.this, ActivityFinish.class);
                startActivity(i);
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            Log.d("ERRORR", "Error " + t.getMessage());
        }
    });
}

//script php insert data
<?php
    $HostName     = "192.168.168.102";
    $HostUser     = "adnan";
    $HostPass     = "123456";
    $DatabaseName = "db_data_dl";
    $con          = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

    $pribadi   = $_POST['pribadi'];
    $kendaraan = $_POST['kendaraan'];
    $data_km   = $_POST['data_km'];

    $mQuery = "INSERT INTO tb_data (pribadi, kendaraan, data_km) VALUES ('$pribadi','$kendaraan','$data_km')";
    if(mysqli_query($con,$mQuery)){
        $result = array("success" => "SUKSES");
        echo json_encode($result, JSON_PRETTY_PRINT);
    }else{
        $result = array("fail" => "FAIL");
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    mysqli_close($con);
?>

//asynctask loader
/*implement*/
LoaderManager.LoaderCallbacks<String[]>

/*create class loader*/
static class msibLoader extends AsyncTaskLoader<String[]>{
    String mKey, mQuery;
    Connection mConn;
    Context mContext;

    public msibLoader(Context context, String key) {
        super(context);
        mContext = context;
        mKey = key;
        Log.d("LOAD", "CONSTRUCTOR key:"+key);
    }

    @Override
    public String[] loadInBackground() {
        Log.d("LOAD", "BACKGROUND");
        if(isConnectBg()){
            Log.d("LOAD", "CONNECT");
            ArrayList<String> listItem = new ArrayList<>();
            try{
                Statement statement = mConn.createStatement();
                mQuery = "SELECT segment1, description  \n" +
                        "FROM mtl_system_items_b\n" +
                        "WHERE segment1 LIKE '"+mKey+"'\n" +
                        "AND rownum < 11\n" +
                        "OR description LIKE '"+mKey+"'\n" +
                        "AND rownum < 11\n" +
                        "GROUP BY segment1, description";
                Log.d("LOAD", mQuery);
                ResultSet result = statement.executeQuery(mQuery);
                while (result.next()){
                    String itemName = result.getString(1);
                    String itemDesc = result.getString(2);
                    listItem.add(itemName+" ~ "+itemDesc);
                }
                Log.d("Array",listItem.toString());
                return listItem.toArray(new String[0]);

            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return new String[]{"Koneksi tidak tersedia. Periksa jaringa dan coba lagi"};
    }

    boolean isConnectBg() {
        Log.d("METHOD", "isConnect");
        mConn = new ManagerSessionUserOracle(mContext).connectDb();

        if (mConn == null) {
            return false;
        }
        return true;
    }
}

/*overide loader callback di activity*/
@Override
public Loader<String[]> onCreateLoader(int id, Bundle args) {
    Log.d("LOADER MANAGER", "CREATE");
    return new msibLoader(this,args.getString("key"));
}

@Override
public void onLoaderReset(Loader<String[]> loader) {
}

@Override
public void onLoadFinished(Loader<String[]> loader, String[] data) {
    Log.d("LOADER MANAGER", "FINISH data length ("+data+"):"+data.length);
    ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.row_list_search,R.id.tv_item,data);
    actv_item.setAdapter(adapter);
    if(data.length>0){
        actv_item.showDropDown();
    }
}

/*start/restart loader parameter dimasukkan ke dalam bundle*/
Bundle bundle = new Bundle();
if(s.length()>0){
    bundle.putString("key",s.toString().toUpperCase()+"%");
}else {
    //jika input kosongan
    bundle.putString("key",s.toString()+"");
}
getSupportLoaderManager().restartLoader(0,bundle,ActivityManageLines.this).forceLoad();

//progressbar dialog
ProgressDialog prog = new ProgressDialog(v.getContext());
prog.setCancelable(false);
prog.setMessage("Sinkronisasi User...");
prog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
prog.setProgress(0);
prog.setMax(100);
prog.show();

//setting kotlin
/*build.gradle app*/
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'

/*build.gradle project*/
/*buildscript*/ ext.kotlin_version = '1.2.30'
/*dependencies*/ classpath "org.jetbrains.kotlin:kotlin-android-extensions:$kotlin_version"

//Anko
//buildscrtip
ext.anko_version = '0.10.5'
//gradle app
implementation "org.jetbrains.anko:anko:$anko_version"
implementation "org.jetbrains.anko:anko-design:$anko_version" //snackbar
implementation "org.jetbrains.anko:anko-recyclerview-v7:$anko_version"
implementation "org.jetbrains.anko:anko-sqlite:$anko_version"

//anko startActivity
startActivity<ActivityDetail>(
"listEvent" to mListEvent,
"index"     to position
)

//anko background task
doAsync {

    uiThread {

    }
}

//gson JSONarray string to array list data
val strJson = ToolBatch().loadAssetJson(context!!, "json/category.json")

val collectionType = object:TypeToken<ArrayList<DataCategory>>(){}.type
val listCategory = Gson().fromJson(strJson, collectionType) as ArrayList<DataCategory>

//convert webp
fun convertWebp(bitmap:Bitmap):File?{
    val file=getOutputMediaFile()
    val bos=ByteArrayOutputStream()

    bitmap.compress(Bitmap.CompressFormat.WEBP,100,bos)
    val bitmapdata=bos.toByteArray()

    //write the bytes in file
    try{
        val fos=FileOutputStream(file)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
    }catch(e:IOException){
        e.printStackTrace()
    }
    return file
}

fun formatHarga(strHarga:String):String{
    val formatUang = NumberFormat.getInstance(Locale.GERMAN)
    val price = formatUang.format(strHarga.toLong())
    return "Rp$price"
}

//Alert Dialog Kotlin
AlertDialog.Builder(this)
.setTitle("Hapus")
.setMessage("Hapus produk ini?")
.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
deleteProduk()
})
.setNegativeButton("Cancel", null).show()

//insert auto increment
//tabel
private final String TAB_GROUP_ADD = "CREATE TABLE groups (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT NOT NULL, description TEXT NOT NULL);";
//content values
ContentValues values = new ContentValues();
values.put(K_TITLE, title);
values.put(K_DESCRIPTION, description);
db.insert(TAB_GROUP, null, values);